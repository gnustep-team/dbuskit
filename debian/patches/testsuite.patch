Description: Switch from UnitKit to GNUstep Make.
 The patch must be updated to Git master before forwarding.
Author: Yavor Doganov <yavor@gnu.org>
Forwarded: no
Last-Update: 2025-03-12
---

--- dbuskit.orig/GNUmakefile.postamble
+++ dbuskit/GNUmakefile.postamble
@@ -27,6 +27,8 @@
 after-clean::
 	@-$(RM) config.make
 	@-$(RM) Source/config.h
+	@-$(RM) Tests/DBusKit
+	-gnustep-tests --clean Tests
 
 # Things to do before distcleaning
 # before-distclean::
@@ -47,3 +49,23 @@
 
 config.make: config.make.in Source/config.h.in
 	./configure
+
+check::
+	cd Tests; ln -s ../Headers DBusKit
+	(\
+	ADDITIONAL_INCLUDE_DIRS="$(ADDITIONAL_OBJCFLAGS)"; \
+	ADDITIONAL_LIB_DIRS="-L../Source/DBusKit.framework/Versions/Current -lDBusKit"; \
+	if [ "$(GNUSTEP_TARGET_OS)" != "windows" -a "$(GNUSTEP_TARGET_OS)" != "mingw32" ]; then \
+	  ADDITIONAL_LDFLAGS="-Wl,-rpath,../Source/DBusKit.framework/Versions/Current"; \
+	  LD_LIBRARY_PATH="../Source/DBusKit.framework/Versions/Current:${LD_LIBRARY_PATH}"; \
+	  export ADDITIONAL_LDFLAGS; \
+	  export LD_LIBRARY_PATH; \
+	fi; \
+	export ADDITIONAL_INCLUDE_DIRS; \
+	export ADDITIONAL_LIB_DIRS; \
+	if [ "$(debug)" = "yes" ]; then \
+	  gnustep-tests --debug Tests; \
+	else \
+	  gnustep-tests Tests; \
+	fi; \
+	)
--- dbuskit.orig/Tests/GNUmakefile
+++ /dev/null
@@ -1,35 +0,0 @@
-include $(GNUSTEP_MAKEFILES)/common.make
-
-#
-# Main framework
-#
-BUNDLE_NAME = DBusKitTests
-
-# Include configuration
-
--include ../config.make
--include ../GNUmakefile.preamble
-
-DBusKitTests_OBJCFLAGS += $(WARN_FLAGS) -I .
-# Treat warnings as errors unless someone chooses to ignore them.
-ifneq ($(nonstrict), yes)
-DBusKitTests_OBJCFLAGS += -Werror
-endif
-
-DBusKitTests_LDFLAGS=-L../Source/DBusKit.framework/$(GNUSTEP_TARGET_LDIR) -lDBusKit -lUnitKit
-
-DBusKitTests_OBJC_FILES += \
-	TestDKArgument.m \
-	TestDKEndpointManager.m \
-        TestDKMethod.m \
-	TestDKMethodCall.m \
-        TestDKPort.m \
-	TestDKProxy.m
-
-include $(GNUSTEP_MAKEFILES)/bundle.make
-
--include GNUmakefile.postamble
-
-# Quick hack to keep gnustep-make from installing the testcase-bundle:
-DBusKitTests.install.bundle.variables::
-	@echo "Not installing testcases"
--- dbuskit.orig/Tests/TestDKArgument.m
+++ dbuskit/Tests/TestDKArgument.m
@@ -21,12 +21,14 @@
 
    */
 #import <Foundation/NSArray.h>
+#import <Foundation/NSConnection.h>
+#import <Foundation/NSDebug.h>
 #import <Foundation/NSDictionary.h>
 #import <Foundation/NSEnumerator.h>
 #import <Foundation/NSString.h>
 #import <Foundation/NSValue.h>
 
-#import <UnitKit/UnitKit.h>
+#import "ObjectTesting.h"
 
 #import "DBusKit/DKProxy.h"
 #import "DBusKit/DKPort.h"
@@ -68,7 +70,7 @@
 }
 @end
 
-@interface TestDKArgument: NSObject <UKTest>
+@interface TestDKArgument: NSObject
 @end
 
 static NSArray *basicSigs;
@@ -103,9 +105,10 @@
  */
 - (void) testRejectInvalid
 {
-  UKNil([[DKArgument alloc] initWithDBusSignature: "k"
+  PASS(![[DKArgument alloc] initWithDBusSignature: "k"
                                              name: nil
-                                           parent: nil]);
+                                           parent: nil],
+       "arguments with an invalid signature returns nil")
 }
 
 /*
@@ -114,9 +117,10 @@
  */
 - (void) testRejectMultiple
 {
-  UKNil([[DKArgument alloc] initWithDBusSignature: "iiu"
+  PASS(![[DKArgument alloc] initWithDBusSignature: "iiu"
                                              name: nil
-                                           parent: nil]);
+                                           parent: nil],
+       "arguments with multiple complete types returns nil")
 }
 
 - (void) testInitializerAssignments
@@ -127,8 +131,8 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "s"
                                                          name: @"Foo"
                                                        parent: fakeParent];
-  UKObjectsEqual(@"Foo",[arg name]);
-  UKObjectsEqual(fakeParent, [arg parent]);
+  PASS_EQUAL(@"Foo", [arg name], "-name works")
+  PASS_EQUAL(fakeParent, [arg parent], "-parent works")
   [arg release];
   [fakeParent release];
 }
@@ -142,7 +146,7 @@
     DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: [sig UTF8String]
                                                           name: nil
                                                         parent: nil];
-    UKObjectsEqual([arg DBusTypeSignature],sig);
+    PASS_EQUAL([arg DBusTypeSignature], sig, "simple round trip works")
     [arg release];
   }
 }
@@ -156,7 +160,8 @@
     DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: [sig UTF8String]
                                                           name: nil
                                                         parent: nil];
-    UKObjectsEqual([basicSigsAndClasses objectForKey: sig] ,[arg objCEquivalent]);
+    PASS_EQUAL([basicSigsAndClasses objectForKey: sig], [arg objCEquivalent],
+               "-objCEquivalent works")
     [arg release];
   }
 }
@@ -166,7 +171,7 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "as"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg DBusTypeSignature], @"as");
+  PASS_EQUAL([arg DBusTypeSignature], @"as", "array type works")
   [arg release];
 }
 
@@ -175,7 +180,7 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "as"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg objCEquivalent], [NSArray class]);
+  PASS_EQUAL([arg objCEquivalent], [NSArray class], "array type is NSArray")
   [arg release];
 }
 
@@ -184,7 +189,7 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "(iiu)"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg DBusTypeSignature], @"(iiu)");
+  PASS_EQUAL([arg DBusTypeSignature], @"(iiu)", "struct type works")
   [arg release];
 }
 
@@ -193,7 +198,7 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "(iiu)"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg objCEquivalent], [NSArray class]);
+  PASS_EQUAL([arg objCEquivalent], [NSArray class], "struct type is NSArray")
   [arg release];
 }
 
@@ -203,7 +208,7 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "v"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg DBusTypeSignature], @"v");
+  PASS_EQUAL([arg DBusTypeSignature], @"v", "variant type works")
   [arg release];
 }
 
@@ -215,7 +220,8 @@
   /* Variant types will be dynamically boxed and unboxed depending on the
    * marshalled value.
    */
-  UKNil([arg objCEquivalent]);
+  PASS(nil == [arg objCEquivalent],
+       "-objCEquivalent returns nil for a variant type")
   [arg release];
 }
 
@@ -226,7 +232,7 @@
                                                                                                      name: nil
                                                                                                    parent: nil];
   DKArgument *arg = [[superArg children] objectAtIndex: 0];
-  UKObjectsEqual(@"{su}",[arg DBusTypeSignature]);
+  PASS_EQUAL(@"{su}", [arg DBusTypeSignature], "dict entry type works")
   [superArg release];
 }
 
@@ -237,7 +243,8 @@
                                                                                                    parent: nil];
   DKArgument *arg = [[superArg children] objectAtIndex: 0];
   // They are also not supposed to carry their own ObjC equivalent class.
-  UKNil([arg objCEquivalent]);
+  PASS(nil == [arg objCEquivalent],
+       "-objCEquivalent returns nil for dict entry type")
   [superArg release];
 }
 
@@ -246,7 +253,8 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "a{su}"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg objCEquivalent], [NSDictionary class]);
+  PASS_EQUAL([arg objCEquivalent], [NSDictionary class],
+             "dictionary is NSDictionary")
   [arg release];
 }
 
@@ -255,7 +263,8 @@
   DKArgument *arg = [[DKArgument alloc] initWithDBusSignature: "(ua{s(iu)}bv)"
                                                          name: nil
                                                        parent: nil];
-  UKObjectsEqual([arg DBusTypeSignature], @"(ua{s(iu)}bv)");
+  PASS_EQUAL([arg DBusTypeSignature], @"(ua{s(iu)}bv)",
+             "nested type works")
   [arg release];
 }
 
@@ -268,15 +277,15 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual(@"Foo", boxedFoo);
+  PASS_EQUAL(@"Foo", boxedFoo, "boxed string works")
 
   if ([arg unboxValue: boxedFoo intoBuffer: &buffer])
   {
-    UKTrue(0 == strcmp(foo,(char*)(uintptr_t)buffer));
+    PASS(0 == strcmp(foo, (char*)(uintptr_t)buffer), "unboxing a string works")
   }
   else
   {
-    UKFail();
+    PASS(0, "unboxing a string works")
   }
 
   [arg release];
@@ -286,11 +295,11 @@
   long long buffer = 0; \
   if ([arg unboxValue: boxedFoo intoBuffer: &buffer]) \
     { \
-      UKTrue(foo == (x)buffer); \
+      PASS(foo == (x)buffer, "unbox of int type works") \
     } \
     else \
     { \
-      UKFail(); \
+      PASS(0, "unbox of int type works") \
     } \
   } while (0)
 
@@ -302,7 +311,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithUnsignedChar: 255], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithUnsignedChar: 255], boxedFoo,
+             "boxing of DBusByte works")
 
   TEST_UNBOX_INTTYPE(unsigned char);
 
@@ -317,7 +327,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithBool: YES], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithBool: YES], boxedFoo,
+             "boxing of DBusBool works")
 
   TEST_UNBOX_INTTYPE(BOOL);
 
@@ -332,7 +343,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithInt: INT16_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithInt: INT16_MAX], boxedFoo,
+             "boxing of DBusInt16 works")
 
   TEST_UNBOX_INTTYPE(int16_t);
 
@@ -346,7 +358,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithUnsignedInt: UINT16_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithUnsignedInt: UINT16_MAX], boxedFoo,
+             "boxing of DBusUInt16 works")
 
   TEST_UNBOX_INTTYPE(uint16_t);
 
@@ -360,7 +373,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithInt: INT32_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithInt: INT32_MAX], boxedFoo,
+             "boxing of DBusInt32 works")
 
   TEST_UNBOX_INTTYPE(int32_t);
 
@@ -374,7 +388,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithUnsignedInt: UINT32_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithUnsignedInt: UINT32_MAX], boxedFoo,
+             "boxing of DBusUInt32 works")
 
   TEST_UNBOX_INTTYPE(uint32_t);
 
@@ -388,7 +403,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithLongLong: INT64_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithLongLong: INT64_MAX], boxedFoo,
+             "boxing of DBusInt64 works")
 
   TEST_UNBOX_INTTYPE(int64_t);
 
@@ -402,7 +418,8 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithUnsignedLongLong: UINT64_MAX], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithUnsignedLongLong: UINT64_MAX], boxedFoo,
+             "boxing of DBusUInt64 works")
 
   TEST_UNBOX_INTTYPE(uint64_t);
 
@@ -417,15 +434,16 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual([NSNumber numberWithDouble: 1.54E+30], boxedFoo);
+  PASS_EQUAL([NSNumber numberWithDouble: 1.54E+30], boxedFoo,
+             "boxing of DBusDouble works")
 
   if ([arg unboxValue: boxedFoo intoBuffer: &buffer])
   {
-    UKFloatsEqual(foo, *(double*)&buffer, 0.0001);
+    PASS(EQ(foo, *(double*)&buffer), "unboxing of DBusDouble works")
   }
   else
   {
-    UKFail();
+    PASS(0, "unboxing of DBusDouble works")
   }
   [arg release];
 }
@@ -439,15 +457,17 @@
                                                          name: nil
                                                        parent: nil];
   id boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKObjectsEqual(@"(ss)", [boxedFoo DBusTypeSignature]);
+  PASS_EQUAL(@"(ss)", [boxedFoo DBusTypeSignature],
+             "boxing of DBusSignature works")
 
   if ([arg unboxValue: boxedFoo intoBuffer: &buffer])
   {
-    UKTrue(0 == strcmp(foo,(char*)(uintptr_t)buffer));
+    PASS(0 == strcmp(foo, (char*)(uintptr_t)buffer),
+         "unboxing of DBusSignature works")
   }
   else
   {
-    UKFail();
+    PASS(0, "unboxing of DBusSignate works")
   }
 
   [arg release];
@@ -470,15 +490,17 @@
                                              name: nil
                                            parent: initialProxy];
   boxedFoo = [arg boxedValueForValueAt: (void*)&foo];
-  UKTrue([boxedFoo isKindOfClass: [DKProxy class]]);
+  PASS([boxedFoo isKindOfClass: [DKProxy class]],
+       "boxed DBusObjectPath is DKProxy")
 
   if ([arg unboxValue: boxedFoo intoBuffer: &buffer])
   {
-    UKTrue(0 == strcmp(foo,(char*)(uintptr_t)buffer));
+    PASS(0 == strcmp(foo, (char*)(uintptr_t)buffer),
+         "unboxing a DBusObjectPAth works")
   }
   else
   {
-    UKFail();
+    PASS(0, "unboxing a DBusObjectPath works")
   }
 
  [arg release];
@@ -507,8 +529,10 @@
                                                                 name: nil
                                                               parent: nil];
   DKArgument *containedArg =  [variantArg DKArgumentWithObject: object];
-  UKObjectsEqual(theSig, [containedArg DBusTypeSignature]);
+  PASS_EQUAL(theSig, [containedArg DBusTypeSignature],
+             "generating an array from DBusSignature for variant type works")
   [object release];
+  [variantArg release];
 }
 
 - (void)testGenerateDictionaryDBusSignatureForVariantType
@@ -516,7 +540,7 @@
   NSNumber *one = [[NSNumber alloc] initWithInt: 89];
   NSNumber *two = [[NSNumber alloc] initWithInt: 5879];
   NSDictionary *object = [[NSDictionary alloc] initWithObjectsAndKeys: one, @"foo", two, @"bar", nil];
-# ifdef __LP64__
+# if 0
   NSString *theSig = @"a{sx}";
 # else
   NSString *theSig = @"a{si}";
@@ -526,7 +550,8 @@
                                                                 name: nil
                                                               parent: nil];
   DKArgument *containedArg =  [variantArg DKArgumentWithObject: object];
-  UKObjectsEqual(theSig, [containedArg DBusTypeSignature]);
+  PASS_EQUAL(theSig, [containedArg DBusTypeSignature],
+             "generating a dict from DBusSignature for variant type works")
   [one release];
   [two release];
   [object release];
@@ -539,7 +564,7 @@
   NSArray *arrayOne = [[NSArray alloc] initWithObjects: one, nil];
   NSArray *arrayTwo = [[NSArray alloc] initWithObjects: two, nil];
   NSDictionary *object = [[NSDictionary alloc] initWithObjectsAndKeys: @"foo", arrayOne, @"bar", arrayTwo, nil];
-# ifdef __LP64__
+# if 0
   NSString *theSig = @"a(axs)";
 # else
   NSString *theSig = @"a(ais)";
@@ -548,7 +573,8 @@
                                                                 name: nil
                                                               parent: nil];
   DKArgument *containedArg =  [variantArg DKArgumentWithObject: object];
-  UKObjectsEqual(theSig, [containedArg DBusTypeSignature]);
+  PASS_EQUAL(theSig, [containedArg DBusTypeSignature],
+             "generating a fallback struct works")
   [arrayOne release];
   [arrayTwo release];
   [one release];
@@ -567,7 +593,8 @@
                                                                 name: nil
                                                               parent: nil];
   DKArgument *containedArg =  [variantArg DKArgumentWithObject: object];
-  UKObjectsEqual(theSig, [containedArg DBusTypeSignature]);
+  PASS_EQUAL(theSig, [containedArg DBusTypeSignature],
+             "generating a variant works")
   [object release];
   [one release];
   [two release];
@@ -575,3 +602,50 @@
 
 
 @end
+
+int
+main (void)
+{
+  START_SET("DKArgument")
+
+  test_alloc(@"DKArgument");
+
+  TestDKArgument *test = [TestDKArgument new];
+  [test testRejectInvalid];
+  [test testRejectMultiple];
+  [test testInitializerAssignments];
+  [test testSimpleRoundtrip];
+  [test testSimpleObjCEquivs];
+  [test testArrayTypeRoundtrip];
+  [test testArrayTypeEquiv];
+  [test testStructTypeRoundtrip];
+  [test testStructTypeEquiv];
+  [test testVariantTypeRoundtrip];
+  [test testVariantTypeEquiv];
+  [test testDictEntryTypeRoundtrip];
+  [test testDictEntryTypeEquiv];
+  [test testDictionaryDetection];
+  [test testNestedTypeRoundTrip];
+  [test testSimpleBoxingRoundtripDBusString];
+  [test testSimpleBoxingDBusByte];
+  [test testSimpleBoxingDBusBool];
+  [test testSimpleBoxingDBusInt16];
+  [test testSimpleBoxingDBusUInt16];
+  [test testSimpleBoxingDBusInt32];
+  [test testSimpleBoxingDBusUInt32];
+  [test testSimpleBoxingDBusInt64];
+  [test testSimpleBoxingDBusUInt64];
+  [test testSimpleBoxingDBusDouble];
+  [test testSimpleBoxingDBusSignature];
+  [test testSimpleBoxingDBusObjectPath];
+  [test testCustomUnboxingSelector];
+  [test testGenerateArrayDBusSignatureForVariantType];
+  [test testGenerateDictionaryDBusSignatureForVariantType];
+  [test testGenerateFallbackStructDBusSignatureForVariantType];
+  [test testGenerateVariantDBusSignatureForVariantType];
+  DESTROY(test);
+
+  END_SET("DKArgument")
+
+  return 0;
+}
--- dbuskit.orig/Tests/TestDKEndpointManager.m
+++ dbuskit/Tests/TestDKEndpointManager.m
@@ -21,7 +21,8 @@
 
    */
 #import <Foundation/NSConnection.h>
-#import <UnitKit/UnitKit.h>
+#import <Foundation/NSThread.h>
+#import "ObjectTesting.h"
 
 #import "../Source/DKEndpointManager.h"
 #import "../Headers/DKPort.h"
@@ -66,15 +67,16 @@
 {
 
   NSAutoreleasePool *arp = [NSAutoreleasePool new];
-  UKTrue([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(boolMulti:)
+  PASS([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(boolMulti:)
                                                                             target: dummy
                                                                               data: nil
-                                                                     waitForReturn: YES]);
+                                                                           waitForReturn: YES],
+       "-boolReturnForPerformingSelector:target:data:waitForReturn: works with boolMulti:")
   [arp release];
 }
 @end
 
-@interface TestDKEndpointManager: NSObject <UKTest>
+@interface TestDKEndpointManager: NSObject
 @end
 
 @implementation TestDKEndpointManager
@@ -85,20 +87,23 @@
 
 - (void)testGetManager
 {
-  UKNotNil([DKEndpointManager sharedEndpointManager]);
+  PASS(nil != [DKEndpointManager sharedEndpointManager],
+       "+sharedEndpointManager works")
 }
 - (void)testGetThread
 {
-  UKNotNil([[DKEndpointManager sharedEndpointManager] workerThread]);
+  PASS(nil != [[DKEndpointManager sharedEndpointManager] workerThread],
+       "-workerThread works")
 }
 
 - (void)testRingBufferReturn
 {
   DKTestDummy *dummy = [DKTestDummy new];
-  UKTrue([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(boolFunction:)
+  PASS([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(boolFunction:)
                                                                              target: dummy
                                                                                data: nil
-                                                                      waitForReturn: YES]);
+                                                                           waitForReturn: YES],
+       "-boolReturnForPerformingSelector:target:data:waitForReturn: works for boolFunction:")
   [dummy release];
 }
 
@@ -108,15 +113,16 @@
   NSUInteger count = 0;
   for (count = 0; count < 5; count++)
   {
-    UKTrue([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(voidMulti:)
+    PASS([[DKEndpointManager sharedEndpointManager] boolReturnForPerformingSelector: @selector(voidMulti:)
                                                                                target: dummy
                                                                                  data: nil
-                                                                        waitForReturn: NO]);
+                                                                             waitForReturn: NO],
+         "-boolReturnForPerformingSelector:target:data:waitForReturn: works with voidMulti:")
   }
-  UKIntsEqual([dummy callCount], 0);
+  PASS([dummy callCount] == 0, "-callCount returns 0")
   NSLog(@"Sleeping 6 seconds to have all calls complete");
   sleep(6);
-  UKIntsEqual([dummy callCount], 5);
+  PASS([dummy callCount] == 5, "-callCount returns 5")
   [dummy release];
 }
 
@@ -136,7 +142,7 @@
   }
   NSLog(@"Sleeping 6 seconds to have all calls complete");
   sleep(6);
-  UKIntsEqual([dummy callCount], 5);
+  PASS([dummy callCount] == 5, "-callCount returns 5")
   [dummy release];
   for (count = 0; count < 5; count++)
   {
@@ -147,3 +153,21 @@
   free(callers);
 }
 @end
+
+int
+main (void)
+{
+  START_SET("DKEndpointManager")
+
+  TestDKEndpointManager *test = [TestDKEndpointManager new];
+  [test testGetManager];
+  [test testGetThread];
+  [test testRingBufferReturn];
+  [test testRingBufferAsync];
+  [test testRingBufferMultiProducer];
+  DESTROY(test);
+
+  END_SET("DKEndpointManager")
+
+  return 0;
+}
--- dbuskit.orig/Tests/TestDKMethodCall.m
+++ dbuskit/Tests/TestDKMethodCall.m
@@ -21,10 +21,11 @@
 
    */
 #import <Foundation/NSConnection.h>
+#import <Foundation/NSDebug.h>
 #import <Foundation/NSInvocation.h>
 #import <Foundation/NSMethodSignature.h>
 #import <Foundation/NSString.h>
-#import <UnitKit/UnitKit.h>
+#import "ObjectTesting.h"
 
 #import "DBusKit/DKPort.h"
 #import "DBusKit/DKProxy.h"
@@ -33,7 +34,7 @@
 #import "../Source/DKMethodCall.h"
 #import "../Source/DKMethod.h"
 
-@interface TestDKMethodCall: NSObject <UKTest>
+@interface TestDKMethodCall: NSObject
 @end
 
 @interface NSObject (FakeIntrospectionSelector)
@@ -58,13 +59,31 @@
   call = [[DKMethodCall alloc] initWithProxy: aProxy
                                       method: [_DKInterfaceIntrospectable DBusMethodForSelector: @selector(Introspect)]
                                   invocation: inv];
-  UKNotNil(call);
+  PASS(nil != call, "-initWithProxy:method:invocation: works")
   [call sendSynchronously];
 
-  UKDoesNotRaiseException([inv getReturnValue: &returnValue]);
-  UKNotNil(returnValue);
-  UKTrue([returnValue isKindOfClass: [NSString class]]);
-  UKTrue([returnValue length] > 0);
+  PASS_RUNS([inv getReturnValue: &returnValue],
+            "-getReturnValue: does not raise an exception")
+  PASS(nil != returnValue, "return value is not nil")
+  PASS([returnValue isKindOfClass: [NSString class]],
+       "return value is NSString")
+  PASS([returnValue length] > 0, "return value's length is greater than zero")
   [call release];
 }
 @end
+
+int
+main (void)
+{
+  START_SET("DKMethodCall")
+
+  test_alloc(@"DKMethodCall");
+
+  TestDKMethodCall *test = [TestDKMethodCall new];
+  [test testMethodCall];
+  DESTROY(test);
+
+  END_SET("DKMethodCall")
+
+  return 0;
+}
--- dbuskit.orig/Tests/TestDKMethod.m
+++ dbuskit/Tests/TestDKMethod.m
@@ -22,14 +22,14 @@
    */
 #import <Foundation/NSMethodSignature.h>
 #import <Foundation/NSNull.h>
-#import <UnitKit/UnitKit.h>
+#import "ObjectTesting.h"
 
 #import "../Source/DKMethod.h"
 #import "../Source/DKInterface.h"
 #import "../Source/DKProxy+Private.h"
 
 #include <string.h>
-@interface TestDKMethod: NSObject <UKTest>
+@interface TestDKMethod: NSObject
 @end
 
 @implementation TestDKMethod
@@ -47,8 +47,8 @@
   NSNull *dummyParent = [NSNull null];
   DKMethod *method = [[DKMethod alloc] initWithName: @"Fooify"
                                              parent: dummyParent];
-  UKObjectsEqual(@"Fooify",[method name]);
-  UKObjectsEqual(dummyParent, [method parent]);
+  PASS_EQUAL(@"Fooify", [method name], "-name works")
+  PASS_EQUAL(dummyParent, [method parent], "-parent works")
   [method release];
 }
 
@@ -57,16 +57,19 @@
   DKMethod *method = [_DKInterfaceIntrospectable DBusMethodForSelector: @selector(Introspect)];
   NSMethodSignature *sig = [method methodSignature];
   NSUInteger argCount = [sig numberOfArguments];
-  UKTrue((0 == strcmp([sig methodReturnType], @encode(id))));
+  PASS((0 == strcmp([sig methodReturnType], @encode(id))),
+       "-methodReturnType returns id")
   if (argCount == 2)
   {
-    UKPass();
-    UKTrue((0 == strcmp([sig getArgumentTypeAtIndex: 0], @encode(id))));
-    UKTrue((0 == strcmp([sig getArgumentTypeAtIndex: 1], @encode(SEL))));
+    PASS(1, "(boxed) arguments are 2")
+    PASS((0 == strcmp([sig getArgumentTypeAtIndex: 0], @encode(id))),
+         "(boxed) -getArgumentTypeAtIndex: 0 returns id")
+    PASS((0 == strcmp([sig getArgumentTypeAtIndex: 1], @encode(SEL))),
+         "(boxed) -getArgumentTypeAtIndex: 1 returns SEL")
   }
   else
   {
-    UKFail();
+    PASS(0, "(boxed) arguments are 2")
   }
 }
 - (void)testBuiltInIntrospectSignatureNotBoxed
@@ -74,16 +77,19 @@
   DKMethod *method = [_DKInterfaceIntrospectable DBusMethodForSelector: @selector(Introspect)];
   NSMethodSignature *sig = [method methodSignatureBoxed: NO];
   NSUInteger argCount = [sig numberOfArguments];
-  UKTrue((0 == strcmp([sig methodReturnType], @encode(char*))));
+  PASS((0 == strcmp([sig methodReturnType], @encode(char*))),
+       "-methodReturnType returns char*")
   if (argCount == 2)
   {
-    UKPass();
-    UKTrue((0 == strcmp([sig getArgumentTypeAtIndex: 0], @encode(id))));
-    UKTrue((0 == strcmp([sig getArgumentTypeAtIndex: 1], @encode(SEL))));
+    PASS(1, "(not boxed) arguments are 2")
+    PASS((0 == strcmp([sig getArgumentTypeAtIndex: 0], @encode(id))),
+         "(not boxed) -getArgumentTypeAtIndex: 0 returns id")
+    PASS((0 == strcmp([sig getArgumentTypeAtIndex: 1], @encode(SEL))),
+         "(not boxed) -getArgumentTypeAtIndex: 1 returns SEL")
   }
   else
   {
-    UKFail();
+    PASS(0, "(not boxed) arguments are 2")
   }
 }
 
@@ -91,14 +97,35 @@
 {
 
   DKMethod *method = [_DKInterfaceIntrospectable DBusMethodForSelector: @selector(Introspect)];
-  UKObjectsEqual(@"- (NSString*)Introspect;", [method methodDeclaration]);
+  PASS_EQUAL(@"- (NSString*)Introspect;", [method methodDeclaration],
+             "-methodDeclaration works")
 }
 
 - (void)testReprarentInCopy
 {
   DKInterface *new = [_DKInterfaceIntrospectable copy];
   DKMethod *method = [new DBusMethodForSelector: @selector(Introspect)];
-  UKNotNil(method);
-  UKObjectsEqual(new, [method parent]);
+  PASS(nil != method, "-DBusMethodForSelector: works")
+  PASS_EQUAL(new, [method parent], "reparent in copy works")
 }
 @end
+
+int
+main (void)
+{
+  START_SET("DKMethod")
+
+  test_alloc(@"DKMethod");
+
+  TestDKMethod *test = [TestDKMethod new];
+  [test testInitializerAssignments];
+  [test testBuiltInIntrospectSignatureBoxed];
+  [test testBuiltInIntrospectSignatureNotBoxed];
+  [test testEmitMethodDeclaration];
+  [test testReprarentInCopy];
+  DESTROY(test);
+
+  END_SET("DKMethod")
+
+  return 0;
+}
--- dbuskit.orig/Tests/TestDKPort.m
+++ dbuskit/Tests/TestDKPort.m
@@ -21,12 +21,13 @@
 
    */
 #import <Foundation/NSConnection.h>
-#import <UnitKit/UnitKit.h>
+#import <Foundation/NSDebug.h>
+#import "ObjectTesting.h"
 
 #import "DBusKit/DKPort.h"
 #import "DBusKit/DKProxy.h"
 
-@interface TestDKPort: NSObject <UKTest>
+@interface TestDKPort: NSObject
 @end
 
 @implementation TestDKPort
@@ -38,6 +39,22 @@
   conn = [NSConnection connectionWithReceivePort: [DKPort port]
                                         sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"] autorelease]];
   aProxy = [conn rootProxy];
-  UKNotNil(aProxy);
+  PASS(nil != aProxy, "-rootProxy works")
 }
 @end
+
+int
+main (void)
+{
+  START_SET("DKPort")
+
+  test_alloc(@"DKPort");
+
+  TestDKPort *test = [TestDKPort new];
+  [test testReturnProxy];
+  DESTROY(test);
+
+  END_SET("DKPort")
+
+  return 0;
+}
--- dbuskit.orig/Tests/TestDKProxy.m
+++ dbuskit/Tests/TestDKProxy.m
@@ -20,8 +20,9 @@
    Boston, MA 02111 USA.
 
    */
+#import <Foundation/NSDebug.h>
 #import <Foundation/NSObject.h>
-#import <UnitKit/UnitKit.h>
+#import "ObjectTesting.h"
 #define INCLUDE_RUNTIME_H
 #include "../Source/config.h"
 #undef INCLUDE_RUNTIME_H
@@ -44,7 +45,7 @@
 - (NSDictionary*)_interfaces;
 @end
 
-@interface TestDKProxy: NSObject <UKTest>
+@interface TestDKProxy: NSObject
 @end
 
 @interface NSObject (FakeDBusSelectors)
@@ -63,12 +64,12 @@
   NSAutoreleasePool *arp = [[NSAutoreleasePool alloc] init];
   NS_DURING
   {
-    UKTrue([(id)self NameHasOwner: name]);
+    PASS([(id)self NameHasOwner: name], "-NameHasOwner: returns YES")
   }
   NS_HANDLER
   {
     NSLog(@"Got exception: %@", localException);
-    UKFail();
+    PASS(0, "-NameHasOwner: does not raise an exception")
   }
   NS_ENDHANDLER
   [arp release];
@@ -100,8 +101,9 @@
 
   unmangledSel = [proxy _unmangledSelector: mangledSelector
                                  interface: &interface];
-  UKObjectsEqual(@"GetNameOwner:", NSStringFromSelector(unmangledSel));
-  UKObjectsEqual(@"org.freedesktop.DBus", interface);
+  PASS_EQUAL(@"GetNameOwner:", NSStringFromSelector(unmangledSel),
+             "unmangled selector is correct")
+  PASS_EQUAL(@"org.freedesktop.DBus", interface, "interface is correct")
 }
 
 - (void)testSendIntrospectMessage
@@ -115,9 +117,11 @@
   aProxy = [conn rootProxy];
   returnValue = [aProxy Introspect];
 
-  UKNotNil(returnValue);
-  UKTrue([returnValue isKindOfClass: [NSString class]]);
-  UKTrue([returnValue length] > 0);
+  PASS(nil != returnValue, "-Introspect works")
+  PASS([returnValue isKindOfClass: [NSString class]],
+       "-Introspect returns NSString")
+  PASS([returnValue length] > 0,
+       "length of returned string is greater than zero")
 }
 
 - (void)testBuildMethodCache
@@ -131,8 +135,8 @@
   aProxy = [conn rootProxy];
   [aProxy DBusBuildMethodCache];
   interfaces = [aProxy _interfaces];
-  UKNotNil(interfaces);
-  UKTrue([interfaces count] > 0);
+  PASS(nil != interfaces, "-_interfaces works")
+  PASS([interfaces count] > 0, "number of interfaces is greater than zero")
 }
 
 - (void)testSendGetId
@@ -145,9 +149,11 @@
                                         sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"] autorelease]];
   aProxy = [conn rootProxy];
   returnValue = [aProxy GetId];
-  UKNotNil(returnValue);
-  UKTrue([returnValue isKindOfClass: [NSString class]]);
-  UKTrue([returnValue length] > 0);
+  PASS(nil != returnValue, "-GetId works")
+  PASS([returnValue isKindOfClass: [NSString class]],
+       "-GetId returns an NSString")
+  PASS([returnValue length] > 0,
+       "length of returned string is greater than zero")
 }
 
 - (void)testExceptionOnSecondHello
@@ -158,7 +164,8 @@
   conn = [NSConnection connectionWithReceivePort: [DKPort port]
                                         sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"] autorelease]];
   aProxy = [conn rootProxy];
-  UKRaisesExceptionNamed([aProxy Hello], @"DKDBusRemoteErrorException");
+  PASS_EXCEPTION([aProxy Hello], @"DKDBusRemoteErrorException",
+                 "sending Hello message raises DKDBusRemoteErrorException")
 }
 
 - (void)testUnboxedMethodCall
@@ -171,7 +178,7 @@
                                         sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"] autorelease]];
   aProxy = [conn rootProxy];
   returnValue = [aProxy GetNameOwner: "org.freedesktop.DBus"];
-  UKTrue(NULL != returnValue);
+  PASS(NULL != returnValue, "-GetNameOwner: works")
 }
 
 - (void)testMixedBoxingStateMethodCall
@@ -184,7 +191,7 @@
                                         sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"] autorelease]];
   aProxy = [conn rootProxy];
   returnValue = [aProxy NameHasOwner: @"org.freedesktop.DBus"];
-  UKTrue(returnValue);
+  PASS(returnValue, "-NameHasOwner: returns YES")
 }
 
 - (void)testProxyAtPath
@@ -192,18 +199,21 @@
   NSConnection *conn = nil;
   id aProxy = nil;
   id returnValue = nil;
-  NSWarnMLog(@"This test is an expected failure if the org.freedesktop.Hal service on the system message bus is not available!");
+  NSWarnMLog(@"This test is an expected failure if the system message bus is not available!");
   conn = [NSConnection connectionWithReceivePort: [DKPort systemBusPort]
-                                        sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.Hal"
+                                        sendPort: [[[DKPort alloc] initWithRemote: @"org.freedesktop.DBus"
                                                                             onBus: DKDBusSystemBus] autorelease]];
-  aProxy = [conn proxyAtPath: @"/org/freedesktop/Hal"];
+  aProxy = [conn proxyAtPath: @"/org/freedesktop/DBus"];
 
-  UKDoesNotRaiseException(returnValue = [aProxy Introspect]);
+  PASS_RUNS(returnValue = [aProxy Introspect],
+            "-Introspect does not raise an exception")
 
 
-  UKNotNil(returnValue);
-  UKTrue([returnValue isKindOfClass: [NSString class]]);
-  UKTrue([returnValue length] > 0);
+  PASS(nil != returnValue, "-Introspect works")
+  PASS([returnValue isKindOfClass: [NSString class]],
+       "-Introspect returns an NSString");
+  PASS([returnValue length] > 0,
+       "length of returned string is greater than zero")
 }
 
 
@@ -236,32 +246,66 @@
   sleep(6);
   for (count = 0;count < 5; count++)
   {
-    UKTrue([(NSThread*)[threads objectAtIndex: count] isFinished]);
+    PASS([(NSThread*)[threads objectAtIndex: count] isFinished],
+         "threaded call works for thread %"PRIuPTR"", count)
   }
 }
 
 
 @end
 
-@interface TestDKDBus: NSObject <UKTest>
+@interface TestDKDBus: NSObject
 @end
 @implementation TestDKDBus
 - (void)testGetSessionBus
 {
-  UKNotNil([DKDBus sessionBus]);
+  PASS(nil != [DKDBus sessionBus], "+sessionBus works")
 }
 - (void)testGetSystemBus
 {
-  UKNotNil([DKDBus systemBus]);
+  PASS(nil != [DKDBus systemBus], "+systemBus works")
 }
 
 - (void)useSessionBus
 {
-  UKNotNil([[DKDBus sessionBus] GetId]);
+  PASS(nil != [[DKDBus sessionBus] GetId], "-GetId works on session bus")
 }
 
 - (void)useSystemBus
 {
-  UKNotNil([[DKDBus systemBus] GetId]);
+  PASS(nil != [[DKDBus systemBus] GetId], "-GetId works on system bus")
 }
 @end
+
+int
+main (void)
+{
+  START_SET("DKProxy")
+
+  TestDKProxy *test = [TestDKProxy new];
+  [test testSelectorUnmangling];
+  [test testSendIntrospectMessage];
+  [test testBuildMethodCache];
+  [test testSendGetId];
+  [test testExceptionOnSecondHello];
+  [test testUnboxedMethodCall];
+  [test testMixedBoxingStateMethodCall];
+  [test testProxyAtPath];
+  [test testThreadedMethodCalls];
+  DESTROY(test);
+
+  END_SET("DKProxy")
+
+  START_SET("DKDBus")
+
+  TestDKDBus *test1 = [TestDKDBus new];
+  [test1 testGetSessionBus];
+  [test1 testGetSystemBus];
+  [test1 useSessionBus];
+  [test1 useSystemBus];
+  DESTROY(test1);
+
+  END_SET("DKDBus")
+
+  return 0;
+}
--- /dev/null
+++ dbuskit/Tests/TestInfo
@@ -0,0 +1 @@
+# Dummy comment to make quilt happy.
--- dbuskit.orig/Tests/GNUmakefile.postamble
+++ /dev/null
@@ -1,43 +0,0 @@
-#
-# GNUmakefile.postamble for DBusKit test bundle 
-#
-
-# Things to do before compiling
-before-all::
-	ln -s ../Source/DBusKit.framework/Versions/Current/Headers DBusKit
-
-# Things to do after compiling
-after-all::
-	@-$(RM) DBusKit
-	LD_LIBRARY_PATH=../Source/DBusKit.framework/Versions/Current/$(GNUSTEP_TARGET_LDIR):$$LD_LIBRARY_PATH ukrun $(BUNDLE_NAME).bundle
-
-# Things to do before installing
-# before-install::
-
-# Things to do after installing
-# after-install::
-
-# Things to do before uninstalling
-# before-uninstall::
-
-# Things to do after uninstalling
-# after-uninstall::
-
-# Things to do before cleaning
-# before-clean::
-
-# Things to do after cleaning
-after-clean::
-	@-$(RM) -rf DBusKitTests.bundle
-
-# Things to do before distcleaning
-# before-distclean::
-
-# Things to do after distcleaning
-#after-distclean::
-
-# Things to do before checking
-# before-check::
-
-# Things to do after checking
-# after-check::
