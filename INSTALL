1 Installation Instructions
***************************

1.1 Required software
=====================

As an Objective-C framework, DBusKit depends on recent versions of the
GNUstep Makefile Package and the GNUstep Base Library. Versions 2.4.0
and 1.20.0 (respectively) are known to work. D-Bus support is provided
by libdbus. DBusKit has been verified to work with version 1.2.1 of
libdbus, but might work with earlier versions. To build the testsuite,
the UnitKit framework is required. It is available from the Étoilé
repository
(http://svn.gna.org/svn/etoile/trunk/Etoile/Frameworks/UnitKit/).

   * gnustep-make (>=2.4.0)

   * gnustep-base (>=1.22.0)

   * libdbus (>=1.2.1)

   * UnitKit (optional)

1.2 Build and Install
=====================

To build and install DBusKit type `make install'. This will
automatically configure the framework prior to building and installing.
DBusKit will usually tell your compiler to treat warnings as errors, if
you don't want that behaviour, add the `nonstrict=yes' flag to your
`make'-invocation.

   Copyright (C) 2010 Free Software Foundation

   Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

